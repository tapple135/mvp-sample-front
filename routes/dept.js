/*jshint esversion: 6 */

//--- import libraries
const express = require('express');
const bodyParser = require('body-parser');
const axios = require('axios');
const cookieParser = require('cookie-parser');
const util = require(__BASEDIR+'/util');
//-----------

//---- 기본 library 셋팅
const router = express.Router();
router.use(bodyParser.urlencoded({ extended: false }));
router.use(cookieParser());
//router.use(express.json());

/*
var app = express();
app.use(express.json());
app.post('/profile', function(req, res) => {
  console.log(req.body)
})
*/


//--------------

//--- Login화면 표시
router.get("/dept", (req, res) => {
	getDept(req, res);
});
//--------------

//--- 인증처리 로직
router.post("/dept", (req, res) => {
	getDept(req, res);
});
//----------------

//-- New
router.get("/insert", function (req, res) {
	util.log("부서 등록 화면");

	res.render("dept/deptInsert", {
		mode: "insert"
	});
});

//--Create
router.post("/insert", (req, res) => {
	util.log("새로운 부서 저장");

	let body = req.body;
	
	console.log("req.body : "+JSON.stringify(req.body));
	console.log("body : "+JSON.stringify(body));
	
	let token = req.cookies[__ACCESS_TOKEN_NAME];
	let _headers = {}
	_headers[__ACCESS_TOKEN_NAME] = token;

	util.log("req.params.deptNo : "+req.body.deptNo);
	util.log("req.params.deptName : "+req.body.deptName);
	util.log("token : "+token.toString());
	util.log("_headers : "+_headers.toString());
	
	axios.post(__API_DEPT_URI+"/depts", 
	body,
	{
		headers: _headers
	})
	.then((ret) => {
		let data = ret.data;
		util.log("##### data"+data);
		if (ret.status == 200) {
			if (data == 1) {
				util.log("##### Success to create");
			} else {
				util.log("저장중 ERROR: " + data.msg);
			}
		} else {
			util.log("rest ERROR: " + ret.status);
		}
	})
	.catch((error) => {
		console.error(error);
	})
	.finally(() => {
		//res.redirect("/dept");
		//res.render("dept/dept");
		getDept(req, res);
	});
});


//--- get one data
let getDept = function(req, res) {
	
	util.log("dept Process");
	let body = req.body;
	
	console.log("req.body : "+JSON.stringify(req.body));
	console.log("body : "+JSON.stringify(body));
	
	axios.get(__API_DEPT_URI+"/depts"
	)
	.then((ret) => {

		//let data = JSON.stringify(ret.data);
		let data = ret.data;

		util.log("data : "+JSON.stringify(data));

		if(ret.status == 200) {
			util.log("Success to get entries !");
			res.render("dept/dept", {
				data: data
			});
		} else {
			res.redirect("/dept");
		}
	})
	.catch((error) => {
		console.error(error);
		res.redirect("/dept");
	});	
}


module.exports = router;

